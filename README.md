# Instance Dokuwiki pour [doc.ubuntu-fr.org](https://doc.ubuntu-fr.org/)

## Développement

### Environnement

```
docker-compose up
````

<http://localhost>

### Thème

Le thème se trouve dans le répertoire `src/themes/ubuntu`.
