<?php

$conf['smtp_host'] = getenv('SMTP_HOST');
$conf['smtp_port'] = getenv('SMTP_PORT');
$conf['smtp_ssl']  = getenv('SMTP_SSL');

$conf['localdomain'] = '';

$conf['auth_user'] = getenv('AUTH_USER');
$conf['auth_pass'] = getenv('AUTH_PASS');


$conf['debug'] = 0;
