<?php
/**
 * Default settings for the authpdo plugin
 *
 * @author Andreas Gohr <andi@splitbrain.org>
 */

$conf['debug'] = 0;
$conf['dsn'] = 'mysql:host='.getenv('MYSQL_HOST').';port='.getenv('MYSQL_PORT').';dbname='.getenv('MYSQL_DATABASE');
$conf['user'] = getenv('MYSQL_USER');
$conf['pass'] = getenv('MYSQL_PASSWORD');

/**
 * statement to select a single user identified by its login name
 *
 * input: :user
 * return: user, name, mail, (clear|hash), [uid], [*]
 */
$conf['select-user'] = 'SELECT id AS uid,
    username AS user,
    username AS name,
    password AS hash,
    email AS mail
    FROM forum_users U, forum_groups G
    WHERE U.group_id = G.g_id
    AND G.g_title != "Guest"
    AND username = :user';

/**
 * statement to check the password in SQL, optional when above returned clear or hash
 *
 * input: :user, :clear, :hash, [uid], [*]
 * return: *
 */
//$conf['check-pass'] = '';

/*
$conf['check-pass'] = 'SELECT "id" AS "uid"
    FROM "forum_users"
    WHERE forum_users.group_id = forum_groups.g_id
    AND forum_groups.g_title != "Guest"
    AND "username" = :user
    AND "password" = :clear';
*/
/**
 * statement to select a single user identified by its login name
 *
 * input: :user, [uid]
 * return: group
 */
$conf['select-user-groups'] = 'SELECT g_title AS `group`
    FROM forum_users U, forum_groups G
    WHERE U.group_id = G.g_id
    AND U.id = :uid';

/**
 * Select all the existing group names
 *
 * return: group, [gid], [*]
 */
$conf['select-groups'] = 'SELECT g_id AS gid,
    g_title AS `group`
    FROM forum_groups';

/**
 * Create a new user
 *
 * input: :user, :name, :mail, (:clear|:hash)
 */
$conf['insert-user'] = '';

/**
 * Remove a user
 *
 * input: :user, [:uid], [*]
 */
$conf['delete-user'] = '';

/**
 * list user names matching the given criteria
 *
 * Make sure the list is distinct and sorted by user name. Apply the given limit and offset
 *
 * input: :user, :name, :mail, :group, :start, :end, :limit
 * out: user
 */
$conf['list-users'] = 'SELECT DISTINCT username AS user
    FROM forum_users U, forum_groups G
    WHERE U.id = G.g_id
    AND G.g_title LIKE :group
    AND U.username LIKE :user
    AND U.username LIKE :name
    AND U.email LIKE :mail
    ORDER BY username
    LIMIT :limit
    OFFSET :start';

/**
 * count user names matching the given criteria
 *
 * Make sure the counted list is distinct
 *
 * input: :user, :name, :mail, :group
 * out: count
 */
$conf['count-users'] = 'SELECT COUNT(DISTINCT username) AS `count`
    FROM forum_users U, forum_groups G
    WHERE U.id = G.g_id
    AND G.g_title LIKE :group
    AND U.username LIKE :user
    AND U.username LIKE :name
    AND U.email LIKE :mail';

/**
 * Update user data (except password and user name)
 *
 * input: :user, :name, :mail, [:uid], [*]
 */
$conf['update-user-info'] = '';

/**
 * Update user name aka login
 *
 * input: :user, :newlogin, [:uid], [*]
 */
$conf['update-user-login'] = 'UPDATE forum_users
    SET username = :newlogin
    WHERE id = :uid';

/**
 * Update user password
 *
 * input: :user, :clear, :hash, [:uid], [*]
 */
$conf['update-user-pass'] = 'UPDATE forum_users
    SET password = :hash
    WHERE id = :uid';

/**
 * Create a new group
 *
 * input: :group
 */
$conf['insert-group'] = 'INSERT
    INTO forum_groups (g_title)
    VALUES (:group)';

/**
 * Make user join group
 *
 * input: :user, [:uid], group, [:gid], [*]
 */
$conf['join-group'] = 'UPDATE forum_users
    SET group_id = :gid
    WHERE id = :uid';

/**
 * Make user leave group
 *
 * input: :user, [:uid], group, [:gid], [*]
 */
$conf['leave-group'] = 'SELECT 1';
