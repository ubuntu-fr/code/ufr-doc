# https://hub.docker.com/r/linuxserver/dokuwiki/
FROM lscr.io/linuxserver/dokuwiki:version-2022-07-31a

# Server configuration
COPY ./etc/nginx /config/nginx
COPY ./etc/php /config/php

# Application configuration
COPY ./etc/dokuwiki /app/www/public/conf
COPY ./src/plugins /app/www/public/conf/lib/plugins
COPY ./src/plugins /config/dokuwiki/lib/plugins
COPY ./src/themes/ubuntu /app/www/public/lib/tpl/ubuntu
COPY ./src/themes/ubuntu /config/dokuwiki/lib/tpl/ubuntu
